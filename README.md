# Blind File Framework

Work with your files on iOS devices without worrying about cloud service implementations. Currently supports Dropbox's [Sync API](https://www.dropbox.com/developers/sync).

# Sample Podfile

This pulls the latest version of the BFF library:

```
platform :ios, "7.0"

pod 'Blind-File-Framework', :git => 'https://bitbucket.org/punchthroughdesign/blind-file-framework'
```

# Usage

```
#import "BFFLocalFileManager.h"
#import "BFFDropboxFileManager.h"
```

* Make sure you have set up the [Dropbox Sync API](https://www.dropbox.com/developers/sync) for your application
* 'Open' BFFFiles before you read their contents, and 'close' them before you call makeFileArray again. Names can be read without opening
* Make sure you have "linked" a Dropbox account prior to creating a BFFDropboxFileManager and calling it's makeFileArray method. Try something along these lines:
```
#!objective-c

DBAccount *account = [[DBAccountManager sharedManager] linkedAccount];
if (account) {
    self.dropboxFileArray = [[BFFDropboxFileManager sharedDropboxManager] makeFileArray];
}
```

BFFFileManager and BFFFile are abstract classes that allow you to treat all BFFxxxxFileManagers and Files the same way.

BFFFiles have control over their contents and name (path). BFFFileManagers can create, delete, and list BFFFiles in the default directory (NSDocumentDirectory and [DBPath root]). Use BFFFileManager's `(NSString *)makeFileArray` method to get an array of file objects. Note that BFFFileManagers are intended to be used as singletons.

#Main Bugs and Shortcomings

This framework is a work in progress. Here are a few problems that have not been addressed:

* Only files ending in .ino are returned in makeFileArray. This is easy to change
* BFFDropboxFileManager's makeFileArray only lists "closed" DBFiles, and returns "nil" for open DBFiles. Therefore, before you call makeFileArray, make sure you 'close' all the BFFDropboxFiles you opened.
* * This seems to be an issue with Dropbox's Sync API. One solution might be to 'open' files at the beginning of readFileContents, and 'close' them at the end of the function.
* makeFileArray does not sync DBFile contents; this is done when you use [BFFDropboxFile open]. It's recommended you do this in a background thread
* When you first call makeFileArray, the Dropbox Sync API syncs all (if any) files from a user's dropbox folder for your application. You must do this in a background thread because the first sync process is blocking. This framework does not abstract away that requirement.