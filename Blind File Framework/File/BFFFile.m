//
//  BFFFile.m
//  Blind File Framework
//
//  Created by Connor Dunne on 7/2/14.
//  Copyright (c) 2014 Kestrel Development. All rights reserved.
//

#import "BFFFile.h"

@interface BFFFile ()

@property (nonatomic, assign) BFFProviderType   providerType;
@property (nonatomic, strong) NSString *        path;
@property (nonatomic, strong) NSString *        cachedContents;

@end

@implementation BFFFile

- (instancetype)initWithProviderType:(BFFProviderType)providerType path:(NSString *)path
{
    if (self = [super init]) {
        self.providerType   = providerType;
        self.path           = path;
    }
    
    return self;
}

- (NSString *)description
{
    NSString * providerLabel;
    
    providerLabel = [[BFFFileManager sharedFileManager] labelForType:self.providerType];
    
    return [NSString stringWithFormat:@"Type: %@ Path: %@, contents: %@", providerLabel, self.path, [self contents]];
}

- (NSString *)name
{
    return [[self.path lastPathComponent] stringByDeletingPathExtension];
}

- (NSString *)contents
{
    if (!self.cachedContents) {
        self.cachedContents = [[BFFFileManager sharedFileManager] contentsForType:self.providerType path:self.path];
    }
    
    return self.cachedContents;
}

- (BOOL)createWithPath:(NSString *)newPath error:(NSError *__autoreleasing *)error
{
    [[BFFFileManager sharedFileManager] createForType:self.providerType path:self.path error:error];

    self.path = newPath;
}

- (BOOL)renameToName:(NSString *)name error:(NSError *__autoreleasing *)error
{
    NSString * newPath;
    
    newPath = [[BFFFileManager sharedFileManager] renameForType:self.providerType path:self.path toName:name error:error];
    
    if (newPath) {
        self.path = newPath;
        return YES;
    } else {
        return NO;
    }
}

- (void)saveWithContents:(NSString *)newContents
{
    [[BFFFileManager sharedFileManager] saveContentsForType:self.providerType path:self.path contents:newContents];

    self.cachedContents = newContents;
}

- (void)delete
{
    [[BFFFileManager sharedFileManager] deleteFileForType:self.providerType path:self.path];
}

#pragma mark - Methods primarily used by DropboxFile
- (void)close
{
    [[BFFFileManager sharedFileManager] closeForType:self.providerType path:self.path];
}

- (void)open
{
    [[BFFFileManager sharedFileManager] openForType:self.providerType path:self.path];
}

- (BOOL)isOpen
{
    [[BFFFileManager sharedFileManager] isOpenForType:self.providerType path:self.path];
}

@end
