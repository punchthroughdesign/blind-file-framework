//
//  BFFFile.h
//  Blind File Framework
//
//  Created by Connor Dunne on 7/2/14.
//  Copyright (c) 2014 Kestrel Development. All rights reserved.
//

// NOTE: This is an abstract class. It should never be instantiated directly.

// TODO: Make sure pointers are being passed around, and no files are being duplicated (particularly DBFiles)

#import <Foundation/Foundation.h>
#import "BFFFileManager.h"

@interface BFFFile : NSObject

@property (nonatomic, readonly) BFFProviderType providerType;
@property (nonatomic, readonly) NSString *      path;
@property (nonatomic, readonly) NSString *      name;
@property (nonatomic, readonly) NSString *      contents;

- (instancetype)initWithProviderType:(BFFProviderType)providerType path:(NSString *)path;

- (BOOL)createWithPath:(NSString *)newPath error:(NSError *__autoreleasing *)error;
- (BOOL)renameToName:(NSString *)name error:(NSError *__autoreleasing *)error;
- (void)saveWithContents:(NSString *)newContents;
- (void)close;
- (void)open;
- (BOOL)isOpen;
- (void)delete;

@end
