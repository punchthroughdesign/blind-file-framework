//
//  BFFDropboxProvider.h
//  Blind File Framework
//
//  Created by Connor Dunne on 6/30/14.
//  Copyright (c) 2014 Kestrel Development. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BFFFileManager.h"
#import "BFFFile.h"
#import <Dropbox/Dropbox.h>

@interface BFFDropboxProvider : NSObject <BFFProvider>

@end
