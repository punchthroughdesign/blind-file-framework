//
//  BFFDropboxProvider.m
//  Blind File Framework
//
//  Created by Connor Dunne on 6/30/14.
//  Copyright (c) 2014 Kestrel Development. All rights reserved.
//

#import "BFFDropboxProvider.h"

#import "DDLog.h"
#ifdef DEBUG
static const int ddLogLevel = LOG_LEVEL_VERBOSE;
#else
static const int ddLogLevel = LOG_LEVEL_WARN;
#endif

@interface BFFDropboxProvider ()

@property (nonatomic, strong) DBAccount *           account;
@property (nonatomic, strong) NSMutableDictionary * files;
@end

@implementation BFFDropboxProvider

+ (void)load
{
    [BFFFileManager registerProvider:[BFFDropboxProvider class]];
}

- (id)init
{
    self = [super init];
    if (self) {
        self.files = [NSMutableDictionary dictionaryWithCapacity:20];

        [self enabled];
    }
    return self;
}

- (BFFProviderType)type
{
    return BFFProviderTypeDropbox;
}

- (BOOL)configure:(NSDictionary *)options
{
    return YES;
}

- (BOOL)enabled
{
    BOOL previouslyLinked;
    
    previouslyLinked    = (self.account != nil);
    self.account        = [[DBAccountManager sharedManager] linkedAccount];
    
    if (self.account) {
        if (!previouslyLinked) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                DBFilesystem *filesystem = [[DBFilesystem alloc] initWithAccount:self.account];
                [DBFilesystem setSharedFilesystem:filesystem];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[BFFFileManager sharedFileManager] refresh];
                });
            });
        }

        if ( [DBFilesystem sharedFilesystem].completedFirstSync && ![[DBFilesystem sharedFilesystem] isShutDown] ) {
            return YES;
        } else {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[BFFFileManager sharedFileManager] refresh];
                });
            });
            return NO;
        }
    } else {
        return NO;
    }
}

- (NSUInteger)fileCount
{
    return [self filenames].count;
}

- (NSString *)label
{
    return @"Dropbox";
}

- (BFFFile *)fileAtIndex:(NSUInteger)index
{
    NSString *  filename;
    BFFFile *   file;
    
    if ( index < self.filenames.count ) {
        filename = [self filenames][index];
        file = [[BFFFile alloc] initWithProviderType:self.type path:filename];
    }

    return file;
}

- (NSString *)contentsForPath:(NSString * )path
{
    DBFile * file;
    NSData * data;
    
    file = self.files[path];
    if (file && file.isOpen) {
        data = [file readData:nil];
        return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    }

    return nil;
}

- (void)openForPath:(NSString * )path
{
    DBFile * file;
    DBPath * dbPath;
    
    file = self.files[path];
    if (file.isOpen) {
        return;
    }
    
    dbPath  = [[DBPath alloc] initWithString:path];
    file    = [[DBFilesystem sharedFilesystem] openFile:dbPath error:nil];
    
    if ( !file ) {
        return;
    }
    
    self.files[path] = file;
    [[DBFilesystem sharedFilesystem] addObserver:self forPath:dbPath block:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            NSError *error;
            BFFFile *updatedFile;
            NSInteger index;
            //check for newer status?
            if ( file.newerStatus.cached ) {
                [file update:&error];
                
                updatedFile = [self fileAtIndex:[self.filenames indexOfObject:path]];
                if ( updatedFile ) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:BFFFileUpdatedNotificationName object:updatedFile];
                }
            }
        });
    }];
}

- (void)closeForPath:(NSString * )path
{
    DBFile * file;
    
    file = self.files[path];
    if (file) {
        [[DBFilesystem sharedFilesystem] removeObserver:self];
        [self addFileSystemObserver];
        return [file close];
        [self.files removeObjectForKey:path];
    }
}

- (BOOL)isOpenForPath:(NSString * )path
{
    DBFile * file;
    
    file = self.files[path];
    if (file) {
        return file.isOpen;
    }
    
    return NO;
}

- (void)deleteForPath:(NSString * )path
{
    DBPath * dbPath;
    
    dbPath = [[DBPath alloc] initWithString:path];
    
    if (![[DBFilesystem sharedFilesystem] deletePath:dbPath error:nil]) {
#warning This needs to be handled or delegated.
        DDLogError(@"Error deleting file: %@", path);
    } else {
        [self.files removeObjectForKey:path];
    }
}

- (void)saveForPath:(NSString * )path contents:(NSString *)contents
{
    DBFile * file;
    NSError *error;
    
    file = self.files[path];
    if (!file) {
        [self openForPath:path];
        file = self.files[path];
    }
    
    if (file) {
        if ( ![file writeString:contents error:&error] ) {
#warning This needs to be handled or delegated.
            DDLogError(@"Writing new content failed!");
        }
    }
}

- (BFFFile *)createForPath:(NSString * )path error:(NSError *__autoreleasing *)error
{
    NSMutableDictionary *   userInfo;
    DBError *               dbError;
    DBPath *                dbPath;
    DBFile *                dbFile;
    BFFFile *               file;
    
    DBPath *folderPath;
    NSArray *pathArray;
    
    if (self.files[path]) {
        if (error) {
            userInfo = [NSMutableDictionary dictionary];
            [userInfo setValue:@"File name already taken! (Case Insensitive)" forKey:NSLocalizedDescriptionKey];
            *error = [NSError errorWithDomain:@"com.punchthrough.beanloader" code:FileError_NameConflict userInfo:userInfo];
        }
        return nil;
    }
    
    // Attempt to create and write to file
    
    pathArray = [path pathComponents];
    
    dbPath  = [[DBPath root] childPath:path];
    folderPath = [[DBPath root] childPath:[pathArray firstObject]];

    dbFile  = [[DBFilesystem sharedFilesystem] createFile:dbPath error:&dbError];

    if ( dbError || !dbFile ) {
        *error = dbError;
        return nil;
    } else {
        
        file = [[BFFFile alloc] initWithProviderType:self.type path:path];
        self.files[path] = dbFile;
        return file;
    }
}

- (NSString *)renameForPath:(NSString * )oldPath toName:(NSString *)name error:(NSError *__autoreleasing *)error
{
    DBFile *                dbFile;
    DBError *               dbError;
    DBPath *                path;
    DBPath *                newPath;
    BOOL                    result;
    NSMutableDictionary *   userInfo;
    BOOL                    wasOpen;
    
    wasOpen = NO;
    dbFile  = self.files[path];
    if (dbFile) {
        wasOpen = YES;
        [dbFile close];
    }
    
    path    = [[DBPath alloc] initWithString:oldPath];
    newPath = [[DBPath root] childPath:name];
    result = [[DBFilesystem sharedFilesystem] movePath:path toPath:newPath error:&dbError];
    if (!result) {
        if (error) {
            userInfo = [NSMutableDictionary dictionary];
            [userInfo setValue:@"Rename failed" forKey:NSLocalizedDescriptionKey];
            *error = [NSError errorWithDomain:@"com.punchthrough.beanloader" code:FileError_UnknownError userInfo:userInfo];
        }
        return nil;
    } else {
        if (wasOpen) {
            [self openForPath:[newPath stringValue]];
        }
        
        return [newPath stringValue];
    }
}

- (NSArray *)filenames
{
    NSArray * fileInfos;
    NSMutableArray * filenames;
    NSArray * filteredFilenames;
    DBAccount *account;
    
    if ( ![self enabled] ) {
        account = [[DBAccountManager sharedManager] linkedAccount];
        DBFilesystem *sharedFilesystem = [[DBFilesystem alloc] initWithAccount:account];
        [DBFilesystem setSharedFilesystem:sharedFilesystem];
    }
    fileInfos = [[DBFilesystem sharedFilesystem] listFolder:[DBPath root] error:nil];
    
    
    filenames = [NSMutableArray array];
    for ( DBFileInfo *fileInfo in fileInfos ) {
        if ( fileInfo.isFolder ) {
            [filenames addObjectsFromArray:[self fileNamesInFileInfo:fileInfo]];
        } else {
            [filenames addObject:fileInfo.path.stringValue];
        }
    }
    filteredFilenames   = [filenames filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"pathExtension == %@", @"ino"]];
    
    return filteredFilenames;
}

- (NSArray *)fileNamesInFileInfo:(DBFileInfo *)fileInfo
{
    NSArray *fileInfos;
    
    fileInfos = [[DBFilesystem sharedFilesystem] listFolder:fileInfo.path error:nil];
    
    return [fileInfos valueForKeyPath:@"path.stringValue"];
}

#pragma mark - observer methods

- (void)addFileSystemObserver
{
    [[DBFilesystem sharedFilesystem] addObserver:self forPathAndDescendants:[DBPath root] block:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:BFFFileDirectoryUpdatedNotificationName object:nil];
        });
    }];
}

@end
