//
//  BFFLocalProvider.m
//  Blind File Framework
//
//  Created by Connor Dunne on 7/2/14.
//  Copyright (c) 2014 Kestrel Development. All rights reserved.
//

#import "BFFLocalProvider.h"

@interface BFFLocalProvider ()

@end

@implementation BFFLocalProvider

+ (void)load
{
    [BFFFileManager registerProvider:[BFFLocalProvider class]];
}

- (id)init
{
    self = [super init];
    if (self) {
        self.rootFolder     = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        self.fileManager    = [NSFileManager defaultManager];
    }
    return self;
}

- (BFFProviderType)type
{
    return BFFProviderTypeLocal;
}

- (BOOL)configure:(NSDictionary *)options
{
    return YES;
}

- (BOOL)enabled
{
    return YES;
}

- (NSUInteger)fileCount
{
    return [self filenames].count;
}

- (NSString *)label
{
    return @"Local";
}

- (BFFFile *)fileAtIndex:(NSUInteger)index
{
    NSString *  filename;
    BFFFile *   file;
    NSString *  path;
    
    filename    = [self filenames][index];
    path        = [NSString stringWithFormat:@"%@%@%@", self.rootFolder, @"/", filename];
    file        = [[BFFFile alloc] initWithProviderType:self.type path:path];

    return file;
}

- (NSString *)contentsForPath:(NSString * )path
{
    NSFileHandle *  fileHandle;
    NSData *        data;
    
    fileHandle  = [NSFileHandle fileHandleForReadingAtPath:path];
    data        = [fileHandle readDataToEndOfFile];
    
    [fileHandle closeFile];
    
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

- (BFFFile *)createForPath:(NSString * )path error:(NSError *__autoreleasing *)error
{
    NSMutableDictionary *   userInfo;
    NSString *              fullPath;
    BFFFile *               file;
    
    fullPath = [self.rootFolder stringByAppendingPathComponent:path];
    
    if ([self.fileManager fileExistsAtPath:fullPath]) {
        if (error) {
            userInfo = [NSMutableDictionary dictionary];
            [userInfo setValue:@"File name already taken! (Case Insensitive)" forKey:NSLocalizedDescriptionKey];
            *error = [NSError errorWithDomain:@"com.punchthrough.beanloader" code:FileError_NameConflict userInfo:userInfo];
        }
        return nil;
    } else {
        [self.fileManager createFileAtPath:fullPath contents:nil attributes:nil];
        file = [[BFFFile alloc] initWithProviderType:self.type path:fullPath];
        return file;
    }
}

- (NSString *)renameForPath:(NSString * )oldPath toName:(NSString *)name error:(NSError *__autoreleasing *)error
{
    NSMutableDictionary *   userInfo;
    NSString *              path;
    
    path = [NSString stringWithFormat:@"%@%@%@", self.rootFolder, @"/", name];

    if ([[[self filenames] valueForKey:@"uppercaseString"] containsObject:[path uppercaseString]]) {
        if (error) {
            userInfo = [NSMutableDictionary dictionary];
            [userInfo setValue:@"File name is the same! (Case Sensitive)" forKey:NSLocalizedDescriptionKey];
            *error = [NSError errorWithDomain:@"com.punchthrough.beanloader" code:FileError_SameNameSameCase userInfo:userInfo];
        }
        return nil;
    }
    
    if (![[NSFileManager defaultManager] moveItemAtPath:oldPath toPath:path error:nil]) {
        if (error) {
            userInfo = [NSMutableDictionary dictionary];
            [userInfo setValue:@"File name already taken!" forKey:NSLocalizedDescriptionKey];
            *error = [NSError errorWithDomain:@"com.punchthrough.beanloader" code:FileError_NameConflict userInfo:userInfo];
        }
        return nil;
    }
    
    return path;
}

- (void)saveForPath:(NSString * )path contents:(NSString *)contents
{
    NSFileHandle *  fileHandle;
    NSData *        data;
    
    fileHandle = [NSFileHandle fileHandleForWritingAtPath:path];
    
    [fileHandle truncateFileAtOffset:0];

    data = [contents dataUsingEncoding:NSUTF8StringEncoding];

    [fileHandle writeData:data];
    [fileHandle closeFile];
}

- (void)openForPath:(NSString * )path
{
    // do nothing
}

- (void)closeForPath:(NSString * )path
{
    // do nothing
}

- (BOOL)isOpenForPath:(NSString * )path
{
    return YES;
}

- (void)deleteForPath:(NSString *)path
{
    if (![self.fileManager removeItemAtPath:path error:nil]) {
#warning This needs to be handled or delegated.
        DDLogError(@"Error deleting file: %@", path);
    }
}

- (NSArray *)filenames
{
    NSArray * allFilenames;
    NSArray * filteredFilenames;
    
    allFilenames        = [self.fileManager contentsOfDirectoryAtPath:self.rootFolder error:nil];
    filteredFilenames   = [allFilenames filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"pathExtension == %@", @"ino"]];
    
    return filteredFilenames;
}

@end
