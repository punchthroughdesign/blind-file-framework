//
//  BFFStaticProvider.h
//  Pods
//
//  Created by Zeke Shearer on 12/3/14.
//
//

#import <Foundation/Foundation.h>
#import "BFFLocalProvider.h"

@interface BFFStaticProvider : BFFLocalProvider

@end
