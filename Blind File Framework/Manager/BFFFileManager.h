//
//  BFFFileManager.h
//  Blind File Framework
//
//  Created by Connor Dunne on 7/2/14.
//  Copyright (c) 2014 Kestrel Development. All rights reserved.
//

// NOTE: This is an abstract class. It should never be instantiated directly.

#import <Foundation/Foundation.h>

@class BFFFile;
@protocol BFFProvider;

typedef NS_ENUM(NSInteger, FileError) {
    FileError_NoError = 1,
    FileError_NameConflict,
    FileError_SameNameSameCase,
    FileError_SameNameDiffCase,
    FileError_IllegalCharacters,
    FileError_UnknownError
};

typedef NS_ENUM(NSUInteger, BFFProviderType) {
    BFFProviderTypeLocal,
    BFFProviderTypeDropbox,
    BFFProviderTypeStatic,
    BFFProviderTypeMax
};

FOUNDATION_EXPORT NSString * BFFFileManagerRefreshNotificationName;
extern NSString *BFFFileUpdatedNotificationName;
extern NSString *BFFFileDirectoryUpdatedNotificationName;

@interface BFFFileManager : NSObject

+ (void)registerProvider:(Class)clazz;
+ (BFFFileManager *)sharedFileManager;

- (BOOL)configureProvider:(BFFProviderType)type options:(NSDictionary * )options;
- (void)refresh;
- (NSArray *)types;
- (NSUInteger)fileCountForType:(BFFProviderType)type;
- (NSString *)labelForType:(BFFProviderType)type;
- (NSArray *)fileNamesForType:(BFFProviderType)type;
- (BFFFile *)fileForType:(BFFProviderType)type atIndex:(NSUInteger)index;
- (NSString *)contentsForType:(BFFProviderType)type path:(NSString *)path;
- (void)closeForType:(BFFProviderType)type path:(NSString *)path;
- (void)openForType:(BFFProviderType)type path:(NSString *)path;
- (BOOL)isOpenForType:(BFFProviderType)type path:(NSString *)path;
- (BFFFile *)createForType:(BFFProviderType)type path:(NSString *)path error:(NSError *__autoreleasing *)error;
- (NSString *)renameForType:(BFFProviderType)type path:(NSString *)path toName:(NSString *)name error:(NSError *__autoreleasing *)error;
- (void)saveContentsForType:(BFFProviderType)type path:(NSString *)path contents:(NSString *)contents;
- (void)deleteFileForType:(BFFProviderType)type path:(NSString *)path;

@end

@protocol BFFProvider <NSObject>

- (BFFProviderType)type;
- (BOOL)configure:(NSDictionary *)options;
- (BOOL)enabled;
- (NSArray *)filenames;
- (NSUInteger)fileCount;
- (NSString *)label;
- (BFFFile *)fileAtIndex:(NSUInteger)index;
- (NSString *)contentsForPath:(NSString * )path;
- (void)openForPath:(NSString * )path;
- (void)closeForPath:(NSString * )path;
- (BOOL)isOpenForPath:(NSString * )path;
- (BFFFile *)createForPath:(NSString * )path error:(NSError *__autoreleasing *)error;
- (NSString *)renameForPath:(NSString * )oldPath toName:(NSString *)name error:(NSError *__autoreleasing *)error;
- (void)saveForPath:(NSString * )path contents:(NSString *)contents;
- (void)deleteForPath:(NSString * )path;

@end