//
//  BFFStaticProvider.m
//  Pods
//
//  Created by Zeke Shearer on 12/3/14.
//
//

#import "BFFStaticProvider.h"

@implementation BFFStaticProvider

//overridden methods from local provider

+ (void)load
{
    [BFFFileManager registerProvider:[BFFStaticProvider class]];
}

- (id)init
{
    self = [super init];
    if (self) {
        self.rootFolder = [self.rootFolder stringByAppendingPathComponent:@"Static"];
    }
    return self;
}

- (BFFProviderType)type
{
    return BFFProviderTypeStatic;
}

- (NSString *)label
{
    return @"Examples";
}


@end
