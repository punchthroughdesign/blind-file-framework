//
//  BFFFileManager.m
//  Blind File Framework
//
//  Created by Connor Dunne on 7/2/14.
//  Copyright (c) 2014 Kestrel Development. All rights reserved.
//

// Please note: This is an asbtract class that should be subclassed by file managers

#import "BFFFileManager.h"

@interface BFFFileManager ()

@property (nonatomic, strong) NSMutableDictionary * providers;

@end

NSString * BFFFileManagerRefreshNotificationName = @"BFFFileManagerRefreshNotification";
NSString *BFFFileUpdatedNotificationName = @"BFFFileUpdatedNotificationName";
NSString *BFFFileDirectoryUpdatedNotificationName = @"BFFFileUpdatedNotificationName";

NSMutableArray * providerClasses;

@implementation BFFFileManager

+ (void)registerProvider:(Class)clazz
{
    if (!providerClasses) {
        providerClasses = [NSMutableArray arrayWithCapacity:5];
    }
    [providerClasses addObject:clazz];
}

+ (BFFFileManager *)sharedFileManager
{
    static BFFFileManager * sharedInstance;
    static dispatch_once_t  onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BFFFileManager alloc] init];
        for (Class c in providerClasses) {
            [sharedInstance registerProvider:[[c alloc] init]];
        }
    });
    
    return sharedInstance;
}

- (instancetype)init
{
    if (self = [super init]) {
        self.providers = [NSMutableDictionary dictionaryWithCapacity:3];
    }
    
    return self;
}

- (void)registerProvider:(NSObject<BFFProvider> *)provider
{
    self.providers[@(provider.type)] = provider;
}

- (BOOL)configureProvider:(BFFProviderType)type options:(NSDictionary * )options
{
    NSObject<BFFProvider> * provider;
    
    provider = self.providers[@(type)];
    if (provider) {
        return [provider configure:options];
    }
    
    return NO;
}

- (void)refresh
{
    [[NSNotificationCenter defaultCenter] postNotificationName:BFFFileManagerRefreshNotificationName object:nil];
}

- (NSArray *)types
{
    return [self.providers.allValues filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"enabled == %@", @(YES)]];
}

- (NSString *)labelForType:(BFFProviderType)type
{
    NSObject<BFFProvider> * provider;
    
    provider = self.providers[@(type)];
    
    return [provider label];
}

- (NSUInteger)fileCountForType:(BFFProviderType)type
{
    NSObject<BFFProvider> * provider;
    
    provider = self.providers[@(type)];

    return [provider fileCount];
}

- (NSArray *)fileNamesForType:(BFFProviderType)type
{
    NSObject<BFFProvider> * provider;
    
    provider = self.providers[@(type)];
    
    return [provider filenames];
}

- (BFFFile *)fileForType:(BFFProviderType)type atIndex:(NSUInteger)index
{
    NSObject<BFFProvider> * provider;
    
    provider = self.providers[@(type)];
    
    return [provider fileAtIndex:index];
}

- (NSString *)contentsForType:(BFFProviderType)type path:(NSString *)path
{
    NSObject<BFFProvider> * provider;
    
    provider = self.providers[@(type)];
    
    return [provider contentsForPath:path];
}

- (void)closeForType:(BFFProviderType)type path:(NSString *)path
{
    NSObject<BFFProvider> * provider;
    
    provider = self.providers[@(type)];
    
    [provider closeForPath:path];
}

- (void)openForType:(BFFProviderType)type path:(NSString *)path
{
    NSObject<BFFProvider> * provider;
    
    provider = self.providers[@(type)];
    
    [provider openForPath:path];
}

- (BOOL)isOpenForType:(BFFProviderType)type path:(NSString *)path
{
    NSObject<BFFProvider> * provider;
    
    provider = self.providers[@(type)];
    
    return [provider isOpenForPath:path];
}

- (BFFFile *)createForType:(BFFProviderType)type path:(NSString *)path error:(NSError *__autoreleasing *)error
{
    NSObject<BFFProvider> * provider;
    BFFFile *file;
    
    provider = self.providers[@(type)];
    
    file = [provider createForPath:path error:error];
    [self refresh];
    return file;
}

- (NSString *)renameForType:(BFFProviderType)type path:(NSString *)path toName:(NSString *)name error:(NSError *__autoreleasing *)error
{
    NSObject<BFFProvider> * provider;
    NSString *updatedName;
    
    provider = self.providers[@(type)];
    
    updatedName = [provider renameForPath:path toName:name error:error];
    [self refresh];
    return updatedName;
}

- (void)saveContentsForType:(BFFProviderType)type path:(NSString *)path contents:(NSString *)contents
{
    NSObject<BFFProvider> * provider;
    
    provider = self.providers[@(type)];
    
    [provider saveForPath:path contents:contents];
}

- (void)deleteFileForType:(BFFProviderType)type path:(NSString *)path
{
    NSObject<BFFProvider> * provider;
    
    provider = self.providers[@(type)];
    
    [provider deleteForPath:path];
    
    [self refresh];
}

@end
