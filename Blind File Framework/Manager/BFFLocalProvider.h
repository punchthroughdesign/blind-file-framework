//
//  BFFLocalProvider.h
//  Blind File Framework
//
//  Created by Connor Dunne on 7/2/14.
//  Copyright (c) 2014 Kestrel Development. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BFFFileManager.h"
#import "BFFFile.h"
#import "DDLog.h"

#ifdef DEBUG
static const int ddLogLevel = LOG_LEVEL_VERBOSE;
#else
static const int ddLogLevel = LOG_LEVEL_WARN;
#endif

@interface BFFLocalProvider : NSObject <BFFProvider>

//for subclasses
@property (nonatomic, strong) NSString *rootFolder;
@property (nonatomic, strong) NSFileManager *fileManager;

@end
