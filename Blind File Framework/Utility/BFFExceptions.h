//
//  BFFException.h
//  Blind File Framework
//
//  Created by Matthew Lewis on 7/24/14.
//  Copyright (c) 2014 Kestrel Development. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BFFExceptions : NSObject

+ (NSException *)mustOverrideInSubclassException;

@end
