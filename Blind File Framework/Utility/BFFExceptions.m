//
//  BFFException.m
//  Blind File Framework
//
//  Created by Matthew Lewis on 7/24/14.
//  Copyright (c) 2014 Kestrel Development. All rights reserved.
//

#import "BFFExceptions.h"

@implementation BFFExceptions

+ (NSException *)mustOverrideInSubclassException
{
    return [NSException
        exceptionWithName:NSInternalInconsistencyException
                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                 userInfo:nil];
}

@end
