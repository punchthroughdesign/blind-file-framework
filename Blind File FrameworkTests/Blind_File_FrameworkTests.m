//
//  Blind_File_FrameworkTests.m
//  Blind File FrameworkTests
//
//  Created by Matthew Lewis on 8/7/14.
//  Copyright (c) 2014 Punch Through Design. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface Blind_File_FrameworkTests : XCTestCase

@end

@implementation Blind_File_FrameworkTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testSmoke
{
    XCTAssertTrue(YES, @"This test will always pass.");
}

@end
