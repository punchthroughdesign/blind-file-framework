Pod::Spec.new do |s|
  s.name         = "Blind-File-Framework"
  s.version      = "0.1.2"
  s.summary      = "Work with your files without worrying about cloud service implementations."
  s.homepage     = "https://bitbucket.org/punchthroughdesign/blind-file-framework"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author       = { "Connor Dunne" => "cdunne@punchthrough.com",
                     "Matthew Lewis" => "mlewis@punchthrough.com",
                     "Matt Bauer" => "mbauer@punchthrough.com" }
  s.source       = { :git => "git@bitbucket.org:punchthroughdesign/Blind-File-Framework.git", :tag => 'v' + s.version.to_s }
  s.platform     = :ios, '7.0'
  s.source_files = 'Blind File Framework/**/*.{h,m}'
  s.frameworks   = 'Dropbox'
  s.requires_arc = true
  s.dependency 'Dropbox-Sync-API-SDK', '~> 3.0.2'
  s.dependency 'CocoaLumberjack', '~> 1.9.2'
  s.xcconfig     = { 'FRAMEWORK_SEARCH_PATHS' => '"${PODS_ROOT}/Dropbox-Sync-API-SDK/dropbox-ios-sync-sdk-3.0.2"' }
end
